package com.example.myapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import com.example.myapplication.ui.theme.MyApplicationTheme

class Final : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Huertos()
                }
            }
        }
    }
}

@Composable
fun Huertos() {
    var huerto_vacio by remember {
        mutableStateOf(R.drawable.huertovacio)
    }
    var huerto_planta by remember {
        mutableStateOf(R.drawable.huertoconplantas)
    }
    var huerto_fruto by remember {
        mutableStateOf(R.drawable.huertoconfrutos)
    }
    var contador by remember {
        mutableStateOf(1)
    }
    var numero by remember {
        mutableStateOf(0)
    }
    Column() {
        if (contador == 1) {
            Image(painter = painterResource(id = huerto_vacio), contentDescription = "huerto")
        }
        if (contador == 2) {
            Image(painter = painterResource(id = huerto_planta), contentDescription = "huerto")
        }
        if (contador == 3) {
            Image(painter = painterResource(id = huerto_fruto), contentDescription = "huerto")
        }
        Button(
            onClick = {
                for (x in 1 ..1) {
                    contador++
                    if (contador == 1) {
                        huerto_vacio = R.drawable.huertovacio
                    } else if (contador == 2) {
                        huerto_planta = R.drawable.huertoconplantas
                    } else if (contador == 3) {
                        huerto_fruto = R.drawable.huertoconfrutos
                        numero += (1..5).random()
                    }else if(contador==4){
                        contador=1
                    }
                }
            } ) {
            Text(text = "Pulsa Aqui")
        }
        Text("Valor: " + numero.toString())
    }
}